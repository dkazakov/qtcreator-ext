This CMake project builds QtCreator with all its deps inside the Krita's Ubuntu 18.04 docker

1) Install extra packages inside the docker:

```bash
apt install libxcb-shm0-dev libxinerama-dev libxcb-icccm4-dev libxcb-xinerama0-dev libxcb-image0-dev libxcb-render-util0-dev libxcb-xinput-dev
```

2) Configure the project:

```bash
mkdir -p /home/appimage/appimage-workspace/deps/qtcreator
mkdir -p /home/appimage/appimage-workspace/d
cmake -DCMAKE_INSTALL_PREFIX=/home/appimage/appimage-workspace/deps/qtcreator -DEXTERNALS_DOWNLOAD_DIR=/home/appimage/appimage-workspace/d ~/persistent/qtcreator-ext
```

3) Build 

```bash
make -j8 all
```

4) Wait a couple of hours...

5) The package with the name `qtcreator-package.tar.gz` will appear in the build directory :)
