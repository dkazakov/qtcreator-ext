#!/bin/bash

file=$1
rpath=$2

if [[ -f "$file" ]] && ( [[ -x "$file" ]] || [[ $file = *.so* ]] ); then 
    filepath=$(realpath $file)
    fullrpath=$(realpath $rpath)
    relative_rpath=$(realpath --relative-to="$(dirname "$filepath")" "$fullrpath")
    
    existing_rpath=$(patchelf --print-rpath $filepath)
    IFS=':' read -ra rpath_array <<< "$existing_rpath"
    (for e in "${rpath_array[@]}"; do [[ "$e" == "\$ORIGIN/$relative_rpath" ]] && exit 0; done) && has_rpath=1 || has_rpath=0

    echo -n "Trying to fix RPATH for: $file"

    if [[ $has_rpath != 1 ]]; then 
        echo "   fixed!"
        patchelf --set-rpath "\$ORIGIN/$relative_rpath:$existing_rpath" $filepath
    else
        echo ""
    fi
else 
    exit 0
fi
